package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> fridge;
    //private ArrayList<FridgeItem> item;
    Integer max = 20;
    
    public Fridge() {
        this.fridge = new ArrayList<>();
    }  

    public int nItemsInFridge() {
        return fridge.size();
    }

    public void emptyFridge() {
        fridge.clear();
    }

    public int totalSize() {
        return max;
    }
    
    public boolean placeIn(FridgeItem item){
        Integer str = fridge.size(); 
        if (!str.equals(totalSize())) {
            fridge.add(item);
            return true; 
        }
        else {
            return false;
        }
    }

    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    public ArrayList<FridgeItem> removeExpiredFood() {
        LocalDate today = LocalDate.now();
        ArrayList<FridgeItem> expired = new ArrayList<>(); 
        ArrayList<FridgeItem> kopi = new ArrayList<>();
        for (FridgeItem item : fridge) {
            kopi.add(item);
        }
        for (FridgeItem item : kopi) {
            if (item.hasExpired()) {
                expired.add(item);
                fridge.remove(item);
            }
        }
        System.out.println(expired);
        return expired;
    }
}
